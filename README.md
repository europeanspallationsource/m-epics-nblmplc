## Deprecation notice
This repository is no longer active. It has been migrated to:
https://gitlab.esss.lu.se/epics-modules/m-epics-nblmPLC

HOW TO RUN THE PLC IOC ON A CONCURRENT TECHNOLOGY BOARD

PLC: IP Address example: 10.2.176.36
MTCA Concurrent Technology (CT): IP Address example:10.2.176.32 

1. Connect with ssh to the MTCA Concurrent Technology (CT) board
ssh -X ceauser@10.2.176.32

2. Only when start-up or reboot the CT board
ceauser@ics-cea-00: ~$ sudo ifconfig enp13s0f0 10.2.176.32
pwd: ceauser
Then:
ceauser@ics-cea-00: ~$ sudo ifconfig enp13s0f0 up

3. Set environment variable and go to the right directory

ceauser@ics-cea-00: ~$ cd e3-3.15.5
ceauser@ics-cea-00: e3-3.15.5 (master)$ source tools/setenv
ceauser@ics-cea-00: e3-3.15.5 (master)$ cd e3-nblmplc/

4. Compilation
ceauser@ics-cea-00: e3-nblmplc (master)$ make build
ceauser@ics-cea-00: e3-nblmplc (master)$ make install
pwd: ceauser

5. Run the IOC
ceauser@ics-cea-00: e3-nblmplc (master)$ iocsh.bash ./cmds/nblmplc.cmd
